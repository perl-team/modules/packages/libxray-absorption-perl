Source: libxray-absorption-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libmodule-build-perl
Build-Depends-Indep: libchemistry-elements-perl,
                     libmath-spline-perl,
                     libreadonly-perl,
                     libstatistics-descriptive-perl,
                     perl
Standards-Version: 3.9.6
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libxray-absorption-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libxray-absorption-perl.git
Homepage: http://cars9.uchicago.edu/~ravel/software/

Package: libxray-absorption-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libchemistry-elements-perl,
         libmath-spline-perl,
         libreadonly-perl,
         libstatistics-descriptive-perl
Description: x-ray absorption data for the elements
 Xray::Absorption supports access to X-ray absorption data.  It is designed
 to be a transparent interface to absorption data from a variety of
 sources.  Currently, the only sources of data are the 1969 McMaster
 tables, the 1999 Elam tables, the 1993 Henke tables, and the 1995
 Chantler tables.  The Brennan-Cowen implementation of the
 Cromer-Liberman tables is available as a drop-on-top addition to this
 package.  More resources can be added easily.
