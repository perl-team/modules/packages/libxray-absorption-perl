#!/usr/bin/perl

use warnings;
use strict;

use Readonly;
Readonly my $HOST => "http://localhost:4444";

use XMLRPC::Lite;

my $client = XMLRPC::Lite -> proxy($HOST);
#$client -> on_fault( sub { die "transport error" } );

my $rpcdata = $client -> available;
print "Available resources: ", join(" ",@{ $rpcdata->result }), $/;

my $elem = "Cu";
$rpcdata = $client -> call("has", {element=>$elem});
($rpcdata->result) ? print "$elem is in this resource\n"
                   : print "$elem is not in this resource\n" ;
$rpcdata = $client -> has({element=>$elem});
($rpcdata->result) ? print "$elem is in this resource\n"
                   : print "$elem is not in this resource\n" ;


$rpcdata = $client -> weight({element=>$elem});
print "weight of $elem = ", $rpcdata->result, $/;
$rpcdata = $client -> density({element=>$elem});
print "density of $elem = ", $rpcdata->result, $/;
$rpcdata = $client -> gamma({element=>$elem, edge=>"K"});
print "Core-hole lifetime of $elem K edge = ", $rpcdata->result, $/;


$rpcdata = $client -> energy({element => $elem,
			      edge    => "k"});
print "$elem K edge = ", $rpcdata->result, $/;
$rpcdata = $client -> energy({element => $elem,
			      line    => "kalpha1"});
print "$elem Kalpha1 line = ", $rpcdata->result, $/;
$rpcdata = $client -> iupac({line=>"lbeta3"});
print "IUPAC notation for Lbeta3 = ", $rpcdata->result, $/;


print $/;
print "Elam total cross sections at 8700, 8800, 8900, 9000, 9100, 9200:\n";
$rpcdata = $client -> xsec({element  => $elem,
			    energies => [8700, 8800, 8900, 9000, 9100, 9200]}
		      );
print join(" ",@{ $rpcdata->result }), $/;

my $sum = 0;
$rpcdata = $client -> intensity({element => $elem,
				 line    => "kalpha1"});
print "$elem Kalpha1 intensity = ", $rpcdata->result, $/;
$sum += $rpcdata->result;
$rpcdata = $client -> intensity({element => $elem,
				 line    => "kalpha2"});
print "$elem Kalpha2 intensity = ", $rpcdata->result, $/;
$sum += $rpcdata->result;
$rpcdata = $client -> intensity({element => $elem,
				 line    => "kalpha3"});
print "$elem Kalpha3 intensity = ", $rpcdata->result, $/;
$sum += $rpcdata->result;
$rpcdata = $client -> intensity({element => $elem,
				 line    => "kb1"});
print "$elem Kbeta1 intensity = ", $rpcdata->result, $/;
$sum += $rpcdata->result;
$rpcdata = $client -> intensity({element => $elem,
				 line    => "kb2"});
print "$elem Kbeta2 intensity = ", $rpcdata->result, $/;
$sum += $rpcdata->result;
$rpcdata = $client -> intensity({element => $elem,
				 line    => "kb3"});
print "$elem Kbeta3 intensity = ", $rpcdata->result, $/;
$sum += $rpcdata->result;
$rpcdata = $client -> intensity({element => $elem,
				 line    => "kb4"});
print "$elem Kbeta4 intensity = ", $rpcdata->result, $/;
$sum += $rpcdata->result;
$rpcdata = $client -> intensity({element => $elem,
				 line    => "kb5"});
print "$elem Kbeta5 intensity = ", $rpcdata->result, $/;
$sum += $rpcdata->result;
print $sum;

$rpcdata = $client -> incoherent({element  => $elem,
				  energies => [8700, 8800, 8900, 9000, 9100, 9200],
				  resource => "McMaster",
				 } );

print $/;
print "McMaster incoherent cross sections at 8700, 8800, 8900, 9000, 9100, 9200:\n";
print join(" ",@{ $rpcdata->result }), $/;

$rpcdata = $client -> intensity({element  => $elem,
				 line     => "kalpha1",
				 resource => "McMaster"});
print "$elem Kalpha1 intensity = ", $rpcdata->result, $/;
$rpcdata = $client -> intensity({element  => $elem,
				 line     => "kalpha2",
				 resource => "McMaster"});
print "$elem Kalpha2 intensity = ", $rpcdata->result, $/;



$rpcdata = $client -> energy({element => "Ti",
			      edge    => "k"});
print "\nTi K edge = ", $rpcdata->result, $/;
$rpcdata = $client -> next_energy({element  => "Ti",
				   edge     => "K",
				   atoms    => [qw(ba ti o)],
				   });
print "next energy = ", join(" ", @{$rpcdata->result}), $/;

$rpcdata = $client -> parse_formula("PbTiO3");
print "\nPbTiO3 parses as:\n";
my %hash = %{$rpcdata->result};
foreach my $k (keys %hash) { printf("  %-2s  %d\n", $k, $hash{$k}); };

$rpcdata = $client -> parse_formula("(N2)0.78 (O2)0.21 (CO2)0.03 Ar0.01 Kr1e-6 Xe9e-7");
print "Air parses as:\n";
#print $rpcdata->faultstring;
%hash = %{$rpcdata->result};
while  ((my ($k, $v)) = each %hash) {
  printf "  %-2s  %G\n", $k, $v;
};
